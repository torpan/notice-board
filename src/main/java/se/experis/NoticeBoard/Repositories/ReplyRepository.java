package se.experis.NoticeBoard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.NoticeBoard.Models.Reply;

public interface ReplyRepository extends JpaRepository<Reply, Integer> {
    Reply getById(int id);
}
