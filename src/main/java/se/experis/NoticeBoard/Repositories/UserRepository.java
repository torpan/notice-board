package se.experis.NoticeBoard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.NoticeBoard.Models.Post;
import se.experis.NoticeBoard.Models.UserEntity;

import java.util.List;

public interface UserRepository extends JpaRepository<UserEntity, Integer> {
    UserEntity getById(String id);
    //List<Post> findAllByIdAndPostList();

    //boolean existsByUserName(String userId);
}
