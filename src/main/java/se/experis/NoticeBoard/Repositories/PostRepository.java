package se.experis.NoticeBoard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.NoticeBoard.Models.Post;
import se.experis.NoticeBoard.Models.Reply;

import java.util.List;

public interface PostRepository extends JpaRepository<Post, Integer> {
    Post getById(int id);
   // Reply getId(int replyId);
    List<Post> findAllByOrderByPublishedOnDesc();
}
