package se.experis.NoticeBoard.Models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id"
)

public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    public Integer id;

    @Column(nullable=false)
    public String postTitle;

    @Column
    public String postText;

    @Column
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    public java.util.Date publishedOn;

    @OneToMany(mappedBy = "post")
    List<Reply> replyList=new ArrayList<Reply>();

    public Post(Integer id, String postTitle, String postText, Date publishedOn) {
        this.id = id;
        this.postTitle = postTitle;
        this.postText = postText;
        this.publishedOn = publishedOn;
    }
}
