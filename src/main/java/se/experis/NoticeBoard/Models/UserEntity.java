package se.experis.NoticeBoard.Models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id"
)

public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    public Integer id;

    @Column
    public String userName;

    @Column
    public String userPassword;


//    @OneToMany(mappedBy = "User")
//    List<Post> postList=new ArrayList<Post>();


    /*
    public UserEntity(Integer id, String userName, String userPassword) {
        this.id = id;
        this.userName = userName;
        this.userPassword = userPassword;
    }

    public UserEntity() {

    }
  public Integer getId() {
        return id;
    }
    public String getUserName() {
        return userName;
    }


    public void setId(Integer id) {
        this.id = id;
    }



    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    } */
}
