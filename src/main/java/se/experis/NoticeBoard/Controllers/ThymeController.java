package se.experis.NoticeBoard.Controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import se.experis.NoticeBoard.Models.Post;
import se.experis.NoticeBoard.Repositories.PostRepository;
import se.experis.NoticeBoard.Utils.SessionKeeper;
import se.experis.NoticeBoard.Utils.Username;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class ThymeController {

    @Autowired
    private PostRepository postRepository;


    @GetMapping("/login")
    public String login(HttpServletRequest request, HttpSession session, Model model) {
        model.addAttribute("sessionId", session.getId());
        model.addAttribute("loginStatus", SessionKeeper.getInstance().CheckSession(session.getId()));
        model.addAttribute("username", new Username());
        return "login";
    }

    @PostMapping("/savesession")
    //@ResponseBody
    public String addSession(@RequestParam(value="action", required = true) String action,
                             @ModelAttribute("username") Username username,
                             HttpServletRequest request,
                             HttpSession session, Model model) {
        if (action.equals("login")) {
            if (username.value.equals("test")) {
                System.out.println("User exists");
                SessionKeeper.getInstance().AddSession(session.getId());
                return "redirect:/all";
            }
            else
            {
                System.out.println("login failed");
            }
        }
        else if(action.equals("logout")) {
            SessionKeeper.getInstance().RemoveSession(session.getId());
            System.out.println("logged out");
        }
        return "redirect:/login";
    }

    @PostMapping("addpost")
    public ResponseEntity<Post> addPost(HttpSession session, @RequestBody Post post) {
        post = postRepository.save(post);
        HttpStatus resp = HttpStatus.CREATED;
        System.out.println("New post created with id: "+ post.id + session.getId());
        return new ResponseEntity<Post>(post, resp);
    }


    @GetMapping("/all")
    public String getPost(HttpServletRequest request, Model model) {
        List<Post> posts = postRepository.findAllByOrderByPublishedOnDesc();
        model.addAttribute("allposts", posts);
        return "allposts";
    }



    @GetMapping("/edit-view/{id}")
    public String editController(@PathVariable("id") String id, Model model) {
        Post post = postRepository.findById(Integer.parseInt(id)).get();
        model.addAttribute("post", post);
        return "editpost";
    }
}
